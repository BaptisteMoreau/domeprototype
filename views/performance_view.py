from typing import List
from fastapi import Depends, APIRouter, HTTPException

from database import SessionLocal
from managers import performance_manager
from models import performance_model
from sqlalchemy.orm import Session

# Initilisation of the router
router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post(
    "/performance/", 
    response_model=performance_model.Performance, 
    summary="Create an event performance",
    tags=["Event Performance"]
)
def create_performance(performance: performance_model.PerformanceCreate, event_id: str, db: Session = Depends(get_db)) -> performance_model.Performance:
    """
    Create an event performance
    """
    return performance_manager.create_performance(db=db, performance=performance, event_id=event_id)


@router.get(
    "/performance/", 
    response_model=List[performance_model.Performance], 
    summary="Get event performances list",
    tags=["Event Performance"]
)
def read_performance_list(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)) -> List[performance_model.Performance]:
    """
    Get the list of all the event performances
    """
    performance_list = performance_manager.get_performance_list(db, skip=skip, limit=limit)
    return performance_list


@router.get(
    "/performance/{performance_id}", 
    response_model=performance_model.Performance, 
    summary="Get event performance details",
    tags=["Event Performance"]
)
def read_performance(performance_id: int, db: Session = Depends(get_db)) -> performance_model.Performance:
    """
    Get details of an event performance
    """
    db_performance = performance_manager.get_performance(db, performance_id=performance_id)
    if db_performance is None:
        raise HTTPException(status_code=404, detail="Performance not found")
    return db_performance

@router.delete(
    "/performance/{performance_id}", 
    summary="Delete an event performance",
    tags=["Event Performance"]
)
def delete_event(performance_id: int, db: Session = Depends(get_db)) -> bool:
    """
    Delete an event performance
    """
    performance_manager.delete_performance(db, performance_id=performance_id)
    return True


@router.get("/fine/{performance_id}", response_model=float)
def get_fine_price(performance_id: int, db: Session = Depends(get_db)) -> float:
    """
    API Endpoint to get fine price for an event performance

    Args:
        performance_id (int): Event Performance identifier
        db (Session, optional): Database session instance. Defaults to Depends(get_db).

    Raises:
        HTTPException: Not found exception when the event performance not exist

    Returns:
        fine_price: fine price
    """
    fine_price = performance_manager.get_total_fine_price(db, performance_id=performance_id)
    return fine_price