from typing import List
from fastapi import Depends, APIRouter, HTTPException

from database import SessionLocal
from managers import filling_rule_manager
from models import filling_rule_model
from sqlalchemy.orm import Session

# Initilisation of the router
router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post(
    "/filling-rule/", 
    response_model=filling_rule_model.FillingRule,
    summary="Create a filling rule",
    tags=["Filling rule"]
)
def create_filling_rule(filling_rule: filling_rule_model.FillingRuleCreate, db: Session = Depends(get_db)) -> filling_rule_model.FillingRule:
    """
    Create a filling rule
    """
    return filling_rule_manager.create_filling_rule(db=db, filling_rule=filling_rule)


@router.get(
    "/filling-rule/", 
    response_model=List[filling_rule_model.FillingRule], 
    summary="Get filling rules list",
    tags=["Filling rule"]
)
def read_filling_rule_list(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)) -> List[filling_rule_model.FillingRule]:
    """
    Get the list of all the filling rules
    """
    filling_rule_list = filling_rule_manager.get_filling_rule_list(db, skip=skip, limit=limit)
    return filling_rule_list


@router.get(
    "/filling-rule/{filling_rule_id}", 
    response_model=filling_rule_model.FillingRule, 
    summary="Get customer details",
    tags=["Filling rule"]
)
def read_filling_rule(filling_rule_id: int, db: Session = Depends(get_db)) -> filling_rule_model.FillingRule:
    db_filling_rule = filling_rule_manager.get_filling_rule(db, filling_rule_id=filling_rule_id)
    """
    Get the details of a filling rule
    """
    if db_filling_rule is None:
        raise HTTPException(status_code=404, detail="Filling rule not found")
    return db_filling_rule

@router.delete(
    "/filling-rule/{filling_rule_id}", 
    summary="Delete a filling rule",
    tags=["Filling rule"]
)
def delete_filling_rule(filling_rule_id: int, db: Session = Depends(get_db)) -> bool:
    """
    Delete a filling rule
    """
    filling_rule_manager.delete_filling_rule(db, filling_rule_id=filling_rule_id)
    return True