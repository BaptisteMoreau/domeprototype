from typing import List
from fastapi import Depends, APIRouter, HTTPException

from database import SessionLocal
from managers import customer_manager
from models import customer_model
from sqlalchemy.orm import Session

# Initilisation of the router
router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post(
    "/customer/", 
    response_model=customer_model.Customer, 
    summary="Create a customer",
    tags=["Customer"]
)
def create_customer(customer: customer_model.CustomerCreate, performance_id: int, db: Session = Depends(get_db)) -> customer_model.Customer:
    """
    Create a customer with the provided data
    """
    return customer_manager.create_customer(db=db, customer=customer, performance_id=performance_id)


@router.get(
    "/customer/", 
    response_model=List[customer_model.Customer],
    summary="Get the customers list", 
    tags=["Customer"]
)
def read_customer_list(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)) -> List[customer_model.Customer]:
    """
    Get the list of all the customers
    """
    customer_list = customer_manager.get_customer_list(db, skip=skip, limit=limit)
    return customer_list


@router.get(
    "/customer/{customer_id}", 
    response_model=customer_model.Customer, 
    summary="Get customer details",
    tags=["Customer"]
)
def read_customer(customer_id: int, db: Session = Depends(get_db)) -> customer_model.Customer:
    """
    Get the details about a customer
    """
    db_customer = customer_manager.get_customer(db, customer_id=customer_id)
    if db_customer is None:
        raise HTTPException(status_code=404, detail="Customer not found")
    return db_customer

@router.delete(
    "/customer/{customer_id}",
    summary="Delete a customer", 
    tags=["Customer"]
)
def delete_customer(customer_id: int, db: Session = Depends(get_db)) -> bool:
    """
    Delete a customer
    """
    customer_manager.delete_customer(db, customer_id=customer_id)
    return True