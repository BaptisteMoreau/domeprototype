from typing import List
from fastapi import Depends, APIRouter, HTTPException

from database import SessionLocal
from managers import performance_manager, fine_manager
from models import performance_model, fine_model
from sqlalchemy.orm import Session

# Initilisation of the router
router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get(
    "/fine/event/{event_id}", 
    response_model=fine_model.EventFine, 
    summary="Get fine detail for an event",
    tags=["Fine"]
)
def get_fine_detail(event_id: int, db: Session = Depends(get_db)) -> fine_model.EventFine:

    return fine_manager.get_fine_detail(db=db, event_id=event_id)