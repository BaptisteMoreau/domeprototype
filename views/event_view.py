from typing import List
from fastapi import Depends, APIRouter, HTTPException

from database import SessionLocal
from managers import event_manager
from models import event_model
from sqlalchemy.orm import Session

# Initilisation of the router
router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post(
    "/event/", 
    response_model=event_model.Event, 
    summary="Create an event",
    tags=["Event"]
)
def create_event(event: event_model.EventCreate, filling_rule_id: str, db: Session = Depends(get_db)) -> event_model.Event:
    """
    Create an event
    """
    return event_manager.create_event(db=db, event=event, filling_rule_id=filling_rule_id)


@router.get(
    "/event/", 
    response_model=List[event_model.Event],
    summary="Get events list", 
    tags=["Event"]
)
def read_event_list(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)) -> List[event_model.Event]:
    """
    Get the list of all the events
    """
    event_list = event_manager.get_events_list(db, skip=skip, limit=limit)
    return event_list


@router.get(
    "/event/{event_id}", 
    response_model=event_model.Event, 
    summary="Get event details",
    tags=["Event"]
)
def read_event(event_id: int, db: Session = Depends(get_db)) -> event_model.Event:
    """
    Get details of an event
    """
    db_event = event_manager.get_event(db, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")
    return db_event

@router.delete(
    "/event/{event_id}", 
    summary="Delete an event",
    tags=["Event"]
)
def delete_event(event_id: int, db: Session = Depends(get_db)) -> bool:
    """
    Delete an event
    """
    event_manager.delete_event(db, event_id=event_id)
    return True