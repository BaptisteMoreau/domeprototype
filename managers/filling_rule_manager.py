from typing import List
from fastapi import HTTPException
from sqlalchemy.orm import Session

from models import database_models, filling_rule_model


def get_filling_rule(db: Session, filling_rule_id: int) -> database_models.FillingRule:
    """
    Get details of a given filling rule

    Args:
        db (Session): Database session instance
        filling_rule_id (int): Filling rule identifier

    Returns:
        FillingRule: Filling rule details
    """
    return db.query(database_models.FillingRule).filter(database_models.FillingRule.id == filling_rule_id).first()


def get_filling_rule_list(db: Session, skip: int = 0, limit: int = 100) -> List[database_models.FillingRule]:
    """
    Get the filling rule list

    Args:
        db (Session): Database session instance
        skip (int, optional): Number of items to skip from the beginning in the process of retrieving data from the database. Defaults to 0.
        limit (int, optional): Number of items to retreive from the database. Defaults to 100.

    Returns:
        List[Event]: List of customers
    """
    return db.query(database_models.FillingRule).offset(skip).limit(limit).all()


def create_filling_rule(db: Session, filling_rule: filling_rule_model.FillingRuleCreate) -> database_models.FillingRule:
    """
    Create a filling rule

    Args:
        db (Session): Database session instance
        event (FillingRuleCreate): Creation data to create an filling rule

    Returns:
        FillingRule: Details of the created filling rule
    """
    db_filling_rule = database_models.FillingRule(threshold=filling_rule.threshold, fine_price=filling_rule.fine_price )
    db.add(db_filling_rule)
    db.commit()
    db.refresh(db_filling_rule)
    return db_filling_rule

def delete_filling_rule(db: Session, filling_rule_id: int) -> None:
    """
    Delete a given filling rule

    Args:
        db (Session): Database session instance
        filling_rule_id (int): Filling rule identifier

    Raises:
        HTTPException: Not found exception when the filling rule not exist
    """
    db_filling_rule = get_filling_rule(db, filling_rule_id=filling_rule_id)
    if db_filling_rule is None:
        raise HTTPException(status_code=404, detail="Filling rule not found")
    db.delete(db_filling_rule)
    db.commit()