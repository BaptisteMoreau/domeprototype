from models import fine_model, database_models, performance_model
from managers import performance_manager
from sqlalchemy.orm import Session


def get_fine_detail(db: Session, event_id:int) -> fine_model.EventFine:
    res = fine_model.EventFine(total_fine_price=0,fine_detail=[])
    db_perf = db.query(database_models.Performance).filter(database_models.Performance.event_id == event_id).all()
    for perf in db_perf:
        fine_value = performance_manager.get_total_fine_price(db=db, performance_id=perf.id)
        res.total_fine_price += fine_value
        perf_fine = fine_model.PerformanceFine(date=perf.date, fine_price=fine_value)
        res.fine_detail.append(perf_fine)
    return res