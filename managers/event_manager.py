from typing import List
from fastapi import HTTPException
from sqlalchemy.orm import Session

from models import database_models, event_model


def get_event(db: Session, event_id: int) -> database_models.Event:
    """
    Get details of a given event

    Args:
        db (Session): Database session instance
        event_id (int): Event identifier

    Returns:
        Event: Event details
    """
    return db.query(database_models.Event).filter(database_models.Event.id == event_id).first()


def get_events_list(db: Session, skip: int = 0, limit: int = 100) -> List[database_models.Event]:
    """
    Get the event list

    Args:
        db (Session): Database session instance
        skip (int, optional): Number of items to skip from the beginning in the process of retrieving data from the database. Defaults to 0.
        limit (int, optional): Number of items to retreive from the database. Defaults to 100.

    Returns:
        List[Event]: List of customers
    """
    return db.query(database_models.Event).offset(skip).limit(limit).all()


def create_event(db: Session, event: event_model.EventCreate, filling_rule_id: str) -> database_models.Event:
    """
    Create an event

    Args:
        db (Session): Database session instance
        event (EventCreate): Creation data to create an event
        filling_rule_id (int): The identifier of the filling rule where the event will be linked

    Returns:
        Event: Details of the created event
    """
    db_event = database_models.Event(localisation=event.localisation, name=event.name, filling_rule_id=filling_rule_id)
    db.add(db_event)
    db.commit()
    db.refresh(db_event)
    return db_event


def delete_event(db: Session, event_id: int) -> None:
    """
    Delete a given event

    Args:
        db (Session): Database session instance
        event_id (int): Event identifier

    Raises:
        HTTPException: Not found exception when the event not exist
    """
    db_event = get_event(db, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")
    db.delete(db_event)
    db.commit()