from typing import List
from sqlalchemy.orm import Session

from fastapi import HTTPException

from models import database_models, customer_model


def get_customer(db: Session, customer_id: int) -> database_models.Customer:
    """
    Get details of a given customer

    Args:
        db (Session): Database session instance
        customer_id (int): Customer identifier to get details

    Returns:
        Customer: Customer details
    """
    return db.query(database_models.Customer).filter(database_models.Customer.id == customer_id).first()


def get_customer_list(db: Session, skip: int = 0, limit: int = 100) -> List[database_models.Customer]:
    """
    Get the customer list

    Args:
        db (Session): Database session instance
        skip (int, optional): Number of items to skip from the beginning in the process of retrieving data from the database. Defaults to 0.
        limit (int, optional): Number of items to retreive from the database. Defaults to 100.

    Returns:
        List[Customer]: List of customers
    """
    return db.query(database_models.Customer).offset(skip).limit(limit).all()


def create_customer(db: Session, customer: customer_model.CustomerCreate, performance_id: int) -> database_models.Customer:
    """
    Create a customer

    Args:
        db (Session): Database session instance
        customer (CustomerCreate): Creation data to create a customer
        performance_id (int): The identifier a the performance where the customer will be linked

    Returns:
        Customer: Details of the created customer
    """
    db_customer = database_models.Customer(phone_number=customer.phone_number, firstname=customer.firstname, surname=customer.surname, performance_id=performance_id)
    db.add(db_customer)
    db.commit()
    db.refresh(db_customer)
    return db_customer

def delete_customer(db: Session, customer_id: int) -> None:
    """
    Delete a given customer

    Args:
        db (Session): Database session instance
        customer_id (int): Customer identifier

    Raises:
        HTTPException: Not found exception when the customer not exist
    """
    db_customer = get_customer(db, customer_id=customer_id)
    if db_customer is None:
        raise HTTPException(status_code=404, detail="User not found")
    db.delete(db_customer)
    db.commit()