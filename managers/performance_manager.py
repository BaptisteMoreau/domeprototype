from typing import List
from fastapi import HTTPException
from sqlalchemy.orm import Session

from models import database_models, performance_model


def get_performance(db: Session, performance_id: int) -> database_models.Performance:
    """
    Get details of a given event performance

    Args:
        db (Session): Database session instance
        performance_id (int): Event performance identifier

    Returns:
        Performance: Performance details
    """
    return db.query(database_models.Performance).filter(database_models.Performance.id == performance_id).first()


def get_performance_list(db: Session, skip: int = 0, limit: int = 100) -> List[database_models.Performance]:
    """
    Get the event performance list

    Args:
        db (Session): Database session instance
        skip (int, optional): Number of items to skip from the beginning in the process of retrieving data from the database. Defaults to 0.
        limit (int, optional): Number of items to retreive from the database. Defaults to 100.

    Returns:
        List[Performance]: List of event performance
    """
    return db.query(database_models.Performance).offset(skip).limit(limit).all()


def create_performance(db: Session, performance: performance_model.PerformanceCreate, event_id: str):
    db_performance = database_models.Performance(date=performance.date, maximum_ticket=performance.maximum_ticket, event_id=event_id)
    
    db.add(db_performance)
    db.commit()
    db.refresh(db_performance)    
           
    return db_performance


    """
    Create a performance

    Args:
        db (Session): Database session instance
        event (PerformanceCreate): Creation data to create a performance

    Returns:
        Performance: Details of the performance
    """
    db_performance = database_models.Performance(date=performance.date, maximum_ticket=performance.maximum_ticket, event_id=event_id)
    db.add(db_performance)
    db.commit()
    db.refresh(db_performance)
    return db_performance

def delete_performance(db: Session, performance_id: int):
    """
    Delete a given performance

    Args:
        db (Session): Database session instance
        performance_id (int): Performance identifier

    Raises:
        HTTPException: Not found exception when the performance not exist
    """
    db_performance = get_performance(db, performance_id=performance_id)
    if db_performance is None:
        raise HTTPException(status_code=404, detail="Performance not found")
    db.delete(db_performance)
    db.commit()

"""
    Get fine price for a given performance

    Args:
        db (Session): Database session instance
        performance_id (int): Performance identifier

    Raises:
        HTTPException: Not found exception when the performance not exist
    """
def get_total_fine_price(db: Session, performance_id: int) -> float:
    total_booking_ticket = 0
    
    db_performance = get_performance(db, performance_id=performance_id)
    if db_performance is None:
        raise HTTPException(status_code=404, detail="Performance not found")
    
    filling_rule = db_performance.event.filling_rule
    missing_place = int(db_performance.maximum_ticket * filling_rule.threshold) - len(db_performance.customer_list)

    if missing_place > 0:
        return missing_place * filling_rule.fine_price
    
    return 0
    