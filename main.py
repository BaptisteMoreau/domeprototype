from fastapi import FastAPI

from database import engine
from models import database_models
from fastapi.middleware.cors import CORSMiddleware
from views import customer_view, event_view, filling_rule_view, performance_view, fine_view

# Generate the database
database_models.Base.metadata.create_all(bind=engine)

# Inititlisation of the API
app = FastAPI(title="Intensif 01 - API Prototype")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Add routers to the API
app.include_router(customer_view.router)
app.include_router(filling_rule_view.router)
app.include_router(event_view.router)
app.include_router(performance_view.router)
app.include_router(fine_view.router)