from datetime import date
from typing import List
from pydantic import BaseModel, Field
from models.customer_model import Customer
from models.event_model import Event
from datetime import date as Date


class PerformanceBase(BaseModel):
    """
    Describe the common attributes of an event performance
    """
    date: Date = Field(description="Date of the performance")
    maximum_ticket: int = Field(description="Maximum number of ticket available")


class PerformanceCreate(PerformanceBase):
    """
    Describe the attributes needed to create an event performance
    """
    pass


class Performance(PerformanceBase):
    """
    Describe an event performance
    """
    id: int = Field(description="Unique identifier")
    customer_list: List[Customer] = Field(description="Customer list of the performance")
    event: Event = Field(description="Details of the event where the performance belong")

    class Config:
        """
        Config the class to be an ORM class
        """
        orm_mode = True
