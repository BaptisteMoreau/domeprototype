from pydantic import BaseModel, Field
from typing import List
from datetime import date as Date

class PerformanceFine(BaseModel):
    date: Date = Field(description="performance date")
    fine_price: float = Field(description="Fine price for this performance")

class EventFine(BaseModel):
    total_fine_price: float = Field(description="Name of the event")
    fine_detail: List[PerformanceFine] = Field(description="fine detail for all performance of this event")


