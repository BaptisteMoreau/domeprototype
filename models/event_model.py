from typing import List
from pydantic import BaseModel, Field
from models.filling_rule_model import FillingRule


class EventBase(BaseModel):
    """
    Describe the common attributes of an event
    """
    name: str = Field(description="Name of the event")
    localisation: str | None = Field(description="Localisation of the event")


class EventCreate(EventBase):
    """
    Describe the attributes needed to create an event
    """
    pass


class Event(EventBase):
    """
    Describe an event
    """
    
    id: int = Field(description="Unique identifier")
    filling_rule: FillingRule = Field(description="Details of the filling rule linked to the event")

    class Config:
        """
        Config the class to be an ORM class
        """
        orm_mode = True
