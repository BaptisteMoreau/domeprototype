from pydantic import BaseModel, Field


class CustomerBase(BaseModel):
    """
    Describe the common attributes of a customer

    """
    phone_number: str = Field(description="Phone number of the customer")
    firstname: str = Field(description="Firstname of the customer")
    surname: str =  Field(description="Surname of the customer")


class CustomerCreate(CustomerBase):
    """
    Describe the attributes needed to create a customer
    """
    pass


class Customer(CustomerBase):
    """
    Describe a customer
    """
    id: int = Field(description="Unique identifier")
    performance_id: int = Field(description="Identifier of the linked performance of the customer")

    class Config:
        """
        Config the class to be an ORM class
        """
        orm_mode = True
