from datetime import date
from pydantic import BaseModel, Field


class FillingRuleBase(BaseModel):
    """
    Describe the common attributes of a filling rule
    """
    threshold: float = Field(description="Filling threshold")
    fine_price: float = Field(description="Fine to pay per missing person to reach the filling threshold")


class FillingRuleCreate(FillingRuleBase):
    """
    Describe the attributes needed to create a filling rule
    """
    pass


class FillingRule(FillingRuleBase):
    """
    Describe a filling rule
    """
    id: int = Field(description="Unique identifier")

    class Config:
        """
        Config the class to be an ORM class
        """
        orm_mode = True
