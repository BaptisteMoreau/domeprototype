from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, Float
from sqlalchemy.orm import relationship

from database import Base


class Customer(Base):
    """
    Database class describing a customer

    Attributes:
        id (Integer): Unique identifier
        phone_number (String): Phone number of the customer
        firstname (String): Firstname of the customer
        surname (String): Surname of the customer
        performance_id (Integer): Id of the linked performance of the customer
    """
    __tablename__ = "customer"

    id = Column(Integer, primary_key=True, index=True)
    phone_number = Column(String, index=True)
    firstname = Column(String)
    surname = Column(String)
    performance_id = Column(Integer, ForeignKey("performance.id"))


class Event(Base):
    """
    Database class describing an event

    Attributes:
        id (Integer): Unique identifier
        name (String): Name of the event
        localisation (String): Localisation of the event
        filling_rule_id (Integer): Id of a filling rule where the event is linked
        
        performance_list (List[Performance]): Performances list of the event
        filling_rule (FillingRule): Details of the filling rule linked to the event
    """
    __tablename__ = "event"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    localisation = Column(String, index=True)
    filling_rule_id = Column(Integer, ForeignKey("filling_rule.id"))
    
    performance_list = relationship("Performance",back_populates="event")
    filling_rule = relationship("FillingRule")


class FillingRule(Base):
    """
    Database class describing a filling rule

    Attributes:
        id (Integer): Unique identifier
        threshold (Float): Filling threshold 
    """
    __tablename__ = "filling_rule"

    id = Column(Integer, primary_key=True, index=True)
    threshold = Column(Float, index=True)
    fine_price = Column(Float, index=True)


class Performance(Base):
    """
    Database class describing an event performance

    Attributes:
        id (Integer): Unique identifier
        date (Date): Maximum date before threshold verification
        maximum_ticket (Integer): Maximum number of ticket available
        event_id (Integer): Id of the event where the performance belong
        
        customer_list (List[Performance]): Customer list of the performance
        event (FillingRule): Details of the event where the performance belong
    """
    __tablename__ = "performance"

    id = Column(Integer, primary_key=True, index=True)
    date = Column(Date, index=True)
    maximum_ticket = Column(Integer, index=True)
    event_id = Column(Integer, ForeignKey("event.id"))

    customer_list = relationship("Customer")
    event = relationship("Event", back_populates="performance_list")
    
